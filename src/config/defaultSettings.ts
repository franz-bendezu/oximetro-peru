
export default {
  navTheme: 'light', // theme for nav menu
  primaryColor: '#52C41A', // primary color of ant design
  layout: 'topmenu', // nav menu position: `sidemenu` or `topmenu`
  contentWidth: 'Fixed', // layout of content: `Fluid` or `Fixed`, only works when layout is topmenu
  fixedHeader: false, // sticky header
  fixSiderbar: false, // sticky siderbar
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'Oximetro Perú',
  pwa: true,
  iconfontUrl: '',
  production: process.env.NODE_ENV === 'production',
  autoHideHeader: false,
  multiTab: true
}
