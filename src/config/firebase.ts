import firebase from "firebase/app"
import  "firebase/firestore"
const app = firebase.initializeApp({
    apiKey: process.env["FIREBASE_API_KEY "],
    authDomain: process.env["FIREBASE_AUTH_DOMAIN "],
    projectId: process.env["FIREBASE_PROJECT_ID "],
});
const db = app.firestore();
db.enablePersistence({experimentalForceOwningTab:true}).then(() => {
  console.log("Woohoo! Multi-Tab Persistence!");
})
  .catch(function(err) {
    console.error(err)
    if (err.code == 'failed-precondition') {
      // Multiple tabs open, persistence can only be enabled
      // in one tab at a a time.
      // ...
    } else if (err.code == 'unimplemented') {
      // The current browser does not support all of the
      // features required to enable persistence
      // ...
    }
  });

export default app
export {db}
