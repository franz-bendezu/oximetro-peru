import Vue from 'vue'
import {Button, Collapse, PageHeader, Timeline} from 'ant-design-vue'
import '@/styles//global.less'
import {ConfigProvider} from 'ant-design-vue'
import {Icon, Card, Avatar, List, Row, Col} from 'ant-design-vue';
import {Tooltip} from 'ant-design-vue';
import {Result} from 'ant-design-vue';

Vue.use(Tooltip);
Vue.use(Result);
Vue.use(Collapse);
Vue.use(Timeline);
Vue.use(Row);
Vue.use(Col);
Vue.use(PageHeader);
Vue.use(Icon)
Vue.use(Card);
Vue.use(Avatar);
Vue.use(List);
Vue.use(ConfigProvider)
Vue.use(Button)


