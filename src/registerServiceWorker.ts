/* eslint-disable no-console */

import { register } from 'register-service-worker'

if (process.env.NODE_ENV === 'production') {

  self.addEventListener('message', (event) => {
    if (event.data && event.data.type === 'SKIP_WAITING') {
      self.skipWaiting();
    }
  });
 // add this to fool it into thinking its offline read
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    registered (registration: ServiceWorkerRegistration) {
      console.log('Service worker has been registered.');
      setInterval(async () => {
        await registration.update();
      }, 1000 * 60 * 60); // e.g. hourly checks
    },
    cached () {
      console.log('Content has been cached for offline use.')
    },
    updatefound () {
      console.log('New content is downloading.')
    },
    updated (registration: ServiceWorkerRegistration) {
      console.log('New content is available; please refresh.')
      document.dispatchEvent(
        new CustomEvent('swUpdated', { detail: registration })
      )
    },
    offline () {
      console.log('No internet connection found. App is running in offline mode.')
      document.dispatchEvent(
        new CustomEvent('offlineMode')
      )

    },
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  })
}
