
import firebase from 'firebase';
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;
import SnapshotOptions = firebase.firestore.SnapshotOptions;
class Department {
  private _id: string;
  private _name: string;

  constructor(id: string, name: string) {
    this._id = id
    this._name = name
  }

  private static converter = {
    toFirestore: function (department: Department) {
      return {
        name: department.name
      }
    },
    fromFirestore(snapshot: DocumentSnapshot, options: SnapshotOptions) {
      const data: any = snapshot.data(options);
      return new Department(snapshot.id, data.nombre)
    }
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }


}

class Province {
  private _id: string;
  private _name: string;

  constructor(id: string, name: string) {
    this._id = id
    this._name = name
  }

  static converter = {
    toFirestore: function (province: Province) {
      return {
        name: province.name
      }
    },
    fromFirestore(snapshot: DocumentSnapshot, options: SnapshotOptions) {
      const data: any = snapshot.data(options);
      return new Province(snapshot.id, data.nombre)
    }
  }


  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}

class District {
  private _id: string;
  private _name: string;
  private _measuringPoints: Array<any>;

  constructor(id: string, name: string, measuringPoints: Array<any>) {
    this._id = id
    this._name = name
    this._measuringPoints = measuringPoints
  }

  static converter = {
    toFirestore: function (province: District) {
      return {
        name: province.name,
      }
    },
    fromFirestore(snapshot: DocumentSnapshot, options: SnapshotOptions) {
      const data: any = snapshot.data(options);
      const measuringPoints  = [];


      for (const measuringPoint of data.puntosMedicion) {
        measuringPoints .push({id: measuringPoint.id, path: measuringPoint.path})
      }
      return new District(snapshot.id, data.nombre, measuringPoints )
    }
  }


  get measuringPoints(): Array<any> {
    return this._measuringPoints;
  }

  set measuringPoints(value: Array<any>) {
    this._measuringPoints = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }
}

class Geometry {
  private readonly _type: string;
  private readonly _coordinates: any[];

  constructor(type: string, coordinates: Array<any>) {
    this._type = type
    this._coordinates = coordinates
  }

  get type(): string {
    return this._type;
  }

  get coordinates(): any[] {
    return this._coordinates;
  }
}

class Feature {
  private _properties: object;
  private _geometry: Geometry;
  private _type: string;

  constructor(geometry: Geometry, properties: object) {
    this._type = "Feature"
    this._geometry = geometry
    this._properties = properties
  }

  static converter = {
    toFirestore: function (feature: Feature) {
      let coordinates = []
      coordinates = feature.geometry.coordinates.map(
        coordinate => ({...coordinate.map((ordinate: any) => ({...ordinate}))})
      )
      return {
        type: feature._type,
        geometry: {coordinates: coordinates, type: feature.geometry.type},
        properties: feature.properties
      }
    },
    fromFirestore(snapshot: DocumentSnapshot, options: SnapshotOptions) {
      const data: any = snapshot.data(options);
      data.properties.id = snapshot.id;
      return new Feature(data.geometry, data.properties)
    }
  }

  get properties(): object {
    return this._properties;
  }

  set properties(value: object) {
    this._properties = value;
  }

  get geometry(): Geometry {
    return this._geometry;
  }

  set geometry(value: Geometry) {
    this._geometry = value;
  }

  get type(): string {
    return this._type;
  }

  set type(value: string) {
    this._type = value;
  }
}

class FeaturesCollection {

  private _type: "FeatureCollection";
  private _features: Array<Feature>;

  constructor(features: Array<Feature>) {
    this._type = "FeatureCollection";
    this._features = features;
  }

  get features(): Array<Feature> {
    return this._features;
  }

  set features(value: Array<Feature>) {
    this._features = value;
  }
  get type(): "FeatureCollection" {
    return this._type;
  }

  set type(value: "FeatureCollection") {
    this._type = value;
  }
}



export {Province, Department, District, Feature,FeaturesCollection}
