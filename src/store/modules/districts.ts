import {db} from "@/config/firebase";
import {District, Province} from "../model";
import {DistrictsStates} from "@/store/types/DistrictsStates";
import {ActionContext} from "vuex";
import RootState from "@/store/types/RootState";


const districts = {
  namespaced: true,
  state: () => ({
    districts: [],
    province: new Province('4p5CPcnsGur58cULKCtG', 'Lima')
  } as DistrictsStates),
  mutations: {
    setProvince(state: DistrictsStates, province: Province) {
      state.province = province;
    },
    setDistricts(state: DistrictsStates, districts: Array<District>) {
      state.districts = districts;
    },
    addProvince(state: DistrictsStates, district: District) {
      state.districts.push(district);
    }

  },
  actions: {
    updateDeparment({commit}: ActionContext<any, any>, province: Province) {
      commit("setProvince", province)
    },
    syncDistricts: async function ({state, commit, rootGetters}: ActionContext<DistrictsStates, RootState>, options = {
      where: {fieldPath: 'havePoints', opStr: '==', value: true}
    }  as { where: any}) {
      const department = rootGetters['provinces/getDepartment'];
      const querySnapshot = await db.collection("departamentos/" + department.id +
        '/provincias/' + state.province.id + "/distritos").where(
        options.where.fieldPath, options.where.opStr, options.where.value
      ).withConverter(District.converter).get();
      for (const doc1 of querySnapshot.docs) {
        const data = await doc1.data();
        commit("addProvince", data)
      }
    }
  },
  getters: {}
}

export default districts;
