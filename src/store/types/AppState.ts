export default interface AppState {
  sideCollapsed: boolean;
  isMobile: boolean;
  theme: string;
  layout: string;
  contentWidth: string;
  fixedHeader: boolean;
  fixedSidebar: boolean;
  autoHideHeader: boolean;
  color: string;
  weak: boolean;
  multiTab: boolean;
  _antLocale: object;
}
