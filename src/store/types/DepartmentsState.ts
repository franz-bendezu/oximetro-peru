import {Department} from "@/store/model";

export interface DepartmentsState {
    departments: Array<Department>

}
