import {RouteConfig} from "vue-router";

export default interface PermissionState{
  routers: Array<RouteConfig>;
  addRouters: Array<RouteConfig>;
}
