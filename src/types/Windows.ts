export {};
declare global {
  interface Window {
    umi_plugin_ant_themeVar: any;

    skipWaiting(): void;

    offlineMode(): void;
  }
}
